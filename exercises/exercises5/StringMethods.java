package exercises5;

public class StringMethods {
	
	//
	// Below are the methods defined for this exercise
	//   The names are not prescribed in the assignment, so choose
	//   names you like (that are meaningful)
	//
	
	//
	// First one is completed for you:
	//
	
	public static String selfConcat(String s) {
		return reverb(s, 2);
	}
	
	public static String reverb(String s, int x) {
		String t = "";
		for (int j = 0; j < x; ++j) {
			t = t + s;
		}
		return t;
	}
	
	public static String reverb2(String s, int x) {
		String [] t = new String[x] ;
		for (int j = 0; j < x; ++j) {
			t [j] = s;
		}
		return join(t, "");
	}
	
	public static String join(String[] array, String joiner) {
		String ans = array[0];
		for (int i = 1; i < array.length; ++i) {
			ans = ans + joiner + array[i];
		}
		return ans;
	}
	
	public static void main(String[] args) {
		
		//
		// Testing the methods
		//
		System.out.println(selfConcat("echo"));
		System.out.println(reverb("cho", 5));
		System.out.println(reverb2("yo", 5));
	
	
	String s = "What is the news today?";
	String [] words = s.split(" ");
	for (String w : words) {
		System.out.println(w);
	}
System.out.println(join(words, " <<>> "));
}
}