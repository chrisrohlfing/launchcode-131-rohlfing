package lab7;

public class Point {
	
	private double x;
	private double y;
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	
	public Point plus(Vector vect) {
		double newX = this.x + vect.getDeltaX();
		double newY = this.y + vect.getDeltaY();
		Point p = new Point (newX, newY);
		return p;
	}
	// if not, remove get
	public Vector minus(Point po) {
		double vecX = this.x - po.x;
		double vecY = this.y - po.y;
		Vector v = new Vector (vecX, vecY);
		return v;
	}
	
	public double distance(Point po) {
		Vector v2 = minus(po);
		double mag = v2.magnitude();
		return mag;

		//double mag = (Math.sqrt(Math.pow(v2.getDeltaX(), 2) + Math.pow(v2.getDeltaY(), 2)));
		//return mag;
	}
	
	
}
