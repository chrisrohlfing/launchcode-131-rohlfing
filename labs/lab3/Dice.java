package lab3;

import cse131.ArgsProcessor;

public class Dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int sided = ap.nextInt("What sided die are you using?");
		int diceNum = ap.nextInt("How many dice do you have?");
		int rollNum = ap.nextInt("How many times do you want to roll them?");
		int [][] game = new int [diceNum][rollNum];
		int [] sums = new int [rollNum];
		
		for (int rolls = 0; rolls < rollNum; ++rolls) {
			int sum = 0;
			for (int dice = 0; dice < diceNum; ++dice) {
				double roll = Math.random();
				int reroll = (int)((roll * sided) + 1);
				game [dice][rolls] = reroll;
				System.out.print(reroll + " ");
				sum = sum + reroll;
			}
			sums [rolls] = sum;
			System.out.print(" : their total is " + sum);
			System.out.println("");
			
		}
		
		for (int i = diceNum; i < (diceNum * sided) + 1; ++i) {
			int tally = 0;
			for (int j = 0; j < rollNum; ++j) {
				if (sums[j] == i) {
					tally = tally + 1;
				}	
			}
			double frac = ((double)(tally / (double)rollNum));
			int frac2 = (int) (frac * 100);
				System.out.println(i + " occured " + tally + " time(s), which is " + frac2 + "%"); 
		}
}
}