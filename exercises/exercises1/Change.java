package exercises1;

import cse131.ArgsProcessor;

public class Change {

	public static void main(String[] args) {
		//
		// Below, prompt the user to enter a number of pennies
		//
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int numPennies = ap.nextInt("Enter the number of pennies");
		
		//
		// Then, compute and print out how many 
		//    dollars, quarters, dimes, nickels, and pennies
		// should be given in exchange for those pennies, so as to
		// minimize the number of coins (see the videos)
		//
		int d = numPennies / 100;
		int numRemainder = numPennies % 100;
		int q = numRemainder / 25;
		numRemainder = numRemainder % 25;
		int di = numRemainder / 10;
		numRemainder = numRemainder % 10;
		int n = numRemainder / 5;
		int p = numRemainder % 5;
		System.out.println("You have " + d + " $s, " + q + " q's, " + di + " d's, " + n + " n's " + p + " p's!");

	}
// so I did this differently then Ron's tutorial, but it still works so I'm going to submit it as is// 
}
