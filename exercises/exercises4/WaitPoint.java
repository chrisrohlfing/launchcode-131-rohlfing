package exercises4;

import java.awt.Color;

import sedgewick.StdDraw;

public class WaitPoint {

	public static void main(String[] args) {
		// wait for the mouse to be pressed and released
		while (!StdDraw.mousePressed()) {

			StdDraw.pause(100);
		}
		System.out.println("pressed");

		// here, the mouse has been pressed
		while (StdDraw.mousePressed()) {
			StdDraw.pause(100);
		}

		// here the mouse is released


		System.out.println("released");
		double x = StdDraw.mouseX();
		double y = StdDraw.mouseY();
		StdDraw.setPenColor(Color.GREEN);
		StdDraw.setPenRadius(.1);
		StdDraw.point(x, y);



		// draw a point at the location of the mouse
		char key = ' ';
		while (key != 'q') {

			while (!StdDraw.hasNextKeyTyped()) {
				StdDraw.pause(100);
			}
			key = StdDraw.nextKeyTyped();
		}
			if (key == 'q') {
				StdDraw.text(0.5, 0.5, "Farewell!");
			}


		}


		// here, a q has been typed

	}


