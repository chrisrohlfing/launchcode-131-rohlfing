package exercises4;

import java.awt.Color;


import sedgewick.StdDraw;

import sedgewick.Draw;
public class GraphicsDemo {

	public static void main(String[] args) {
		// blue point (look carefully, will be very small on your screen)
		Color c= Color.BLUE;

		StdDraw.setPenColor(c);
		//StdDraw.setPenRadius();
		StdDraw.point(.5, .5);
		
		// larger green point
		StdDraw.setPenColor(Color.GREEN);
		StdDraw.setPenRadius(.01);
		StdDraw.point(.7, .2);
		// unfilled red triangle
		StdDraw.setPenColor(Color.RED);
		StdDraw.line(.2, .6, .5, 0);
		StdDraw.line(.5, 0, .4, .3);
		StdDraw.line(.4, .3, .2, .6);
		
		// yellow circle, filled
		StdDraw.setPenColor(Color.YELLOW);
		StdDraw.filledCircle(.8, .8, .15);

		// filled blue rectangle
		StdDraw.setPenColor(c);
		StdDraw.filledRectangle(.8, .35, .02, .3);
		


	}

}
