package exercises4;

import cse131.ArgsProcessor;

public class Pitches {

	public static void main(String[] args) {
		// encoding has 0 as concert A
		//    1 would be Bb, just above A
		//   -1 would be Ab, just below A
		ArgsProcessor ap = new ArgsProcessor(args);
		int p = ap.nextInt("Pitch?"); 
		int k = ap.nextInt("2nd Pitch");
		double f = 440 * (Math.pow(2, p/12.0));
		double f2 = 440 * (Math.pow(2, k/12.0));
		double ratio = f2 / f;
		
		System.out.println("the frequency of your 1st pitch is " + f);
		System.out.println("and your second is " + f2);
		System.out.println("their ratio is " + ratio);

	}

}
