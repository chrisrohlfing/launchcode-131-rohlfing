package lab4;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class BumpingBalls {
	
	public static void main(String[] args) {

    	ArgsProcessor ap = new ArgsProcessor(args);
    	int balls = ap.nextInt("How many balls");
    	int pause = ap.nextInt("Enter pause time:");
    	

        // set the scale of the coordinate system
        StdDraw.setXscale(-1.0, 1.0);
        StdDraw.setYscale(-1.0, 1.0);

        // initial values
        double [] px = new double [balls];
        double [] py = new double [balls];
        double [] vx = new double [balls];
        double [] vy = new double [balls];
        for (int i = 0; i < balls; ++i) {
        
        	px [i] = (Math.random() * 1.8) - .9;
        	py [i] = (Math.random() * 1.8) - .9;
        	vx [i] = (Math.random() * .09) - .045;
        	vy [i] = (Math.random() * .09) - .045;
        }
        
     
       
        
        // main animation loop
        while (true)  {
        	for (int i = 0; i < balls; ++i) {
        		if (Math.abs(px[i] + vx[i]) > .975) {
        			vx[i] = -vx[i];
        		}
        		if (Math.abs(py[i] + vy[i]) > .975) {
        			vy[i] = -vy[i];
        		}
        	}
        	
        	for (int i = 0; i < balls-1; ++i) {
        		for (int j = i+1; j < balls; ++j) {
        			double d = Math.sqrt((Math.pow(px[j]-px[i], 2))+(Math.pow(py[j]-py[i], 2)));
        			if (d <= .05) {
        				vx[i] = -vx[i];
        				vy[i] = -vy[i];
        				vx[j] = -vx[j];
        				vy[j] = -vy[j];
        			}        				
        		}        		
        	}
        	
       

        	
        	
            /* bounce off wall according to law of elastic collision
            if (Math.abs(rx + vx) > 1.0 - radius) vx = -vx;
            if (Math.abs(ry + vy) > 1.0 - radius) vy = -vy;

          
            

             update position
            rx = rx + vx; 
            ry = ry + vy; 
            */
           

            // clear the background
            StdDraw.setPenColor(StdDraw.GRAY);
            StdDraw.filledSquare(0, 0, 1.0);
            StdDraw.setPenColor(StdDraw.GREEN);
            // draw ball on the screen
            for (int i = 0; i < balls; ++i) {
            	px[i] = px[i] + vx[i];
            	py[i] = py[i] + vy[i];
            	StdDraw.filledCircle(px[i], py[i], .025);
            }
            
            
              
            
            // display and pause for 20 ms
            StdDraw.show(pause); 
        } 
    } 

}
