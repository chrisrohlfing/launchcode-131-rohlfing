package lab1;

import cse131.ArgsProcessor;

public class nutrition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArgsProcessor ap = new ArgsProcessor(args);
String name = ap.nextString("What is the food?");
double carbs = ap.nextDouble("How many carbs?");
double fat = ap.nextDouble("How many grams of fat?");
double pro = ap.nextDouble("How many grams of protein?");
double cal = ap.nextDouble("How many calories?");



double ccarbs = (carbs * 4);
ccarbs *= 10;
ccarbs = Math.round(ccarbs);
ccarbs /= 10;
double cfat = (fat * 9);
cfat *= 10;
cfat = Math.round(cfat);
cfat /= 10; 
double cpro = (pro * 4);
cpro *= 10;
cpro = Math.round(cpro);
cpro /= 10;

double avcarbs = 100 * (ccarbs / cal);
double avfat = 100 * (cfat / cal);
double avpro = 100 * (cpro/ cal);

avfat = avfat * 10;
avfat = Math.round(avfat);
avfat = avfat / 10;
avpro = avpro * 10;
avpro = Math.round(avpro);
avpro = avpro / 10;
avcarbs = avcarbs * 10;
avcarbs = Math.round(avcarbs);
avcarbs = avcarbs / 10;


double fiber = (ccarbs + cfat + cpro) - cal;
double fiber2 = fiber / 4;

System.out.println(name + " has " + carbs + " grams of carbohydrates = " + ccarbs + " Calories.");
System.out.println(name + " has " + fat + " grams of fat = " + cfat + " Calories.");
System.out.println(name + " has " + pro + " grams of protein = " + cpro + " Calories.");
System.out.println(avcarbs + "% of your food is carbohydrates");
System.out.println(avfat + "% of your food is fat");
System.out.println(avpro + "% of your food is protein");
System.out.println("grams of unavailable calories: " + fiber + " - or " + fiber2 + " grams of fiber");
		
		
boolean x = (avcarbs < 25) ? true : false;
System.out.println("This food is acceptable for a low-carb diet? " + x);
boolean y = (avfat < 15) ? true : false;
System.out.println("This food is acceptable for a low-carb diet? " + y);
boolean head = (Math.random() < .5);
System.out.println("By coin flip, you should eat this food? " + head);

	}

}
