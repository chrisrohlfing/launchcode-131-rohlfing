package lab7;

public class Vector {
	final private double deltaX;
	final private double deltaY;


	public Vector(double deltaX, double deltaY) {
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}

	public double getDeltaX() {
		return deltaX;
	}

	public double getDeltaY() {
		return deltaY;
	}


	public String toString() {
		return "[" + deltaX + "" + deltaY + "]";
	}

	public double magnitude() {
		double ans = (Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)));
		return ans;
	}

	public Vector deflectX() {
		Vector v = new Vector(-deltaX, deltaY);
		return v;
	}

	public Vector deflectY() {
		Vector v = new Vector(deltaX, -deltaY);
		return v;
	}

	public Vector plus(Vector vect) {
		double newX = this.deltaX + vect.deltaX;
		double newY = this.deltaY + vect.deltaY;
		Vector v = new Vector(newX, newY);
		return v;
		//return new Vector(newX, newY);
	}

	public Vector minus(Vector vect) {
		double newX = this.deltaX - vect.deltaX;
		double newY = this.deltaY - vect.deltaY;
		Vector v = new Vector(newX, newY);
		return v;
		//return new Vector(newX, newY);
	}

	public Vector scale(double factor) {
		double facX = this.deltaX * factor;
		double facY = this.deltaY * factor;
		Vector v = new Vector(facX, facY);
		return v;		
	}

	public Vector rescale(double magnitude) {
		if (this.magnitude() == 0) {
			Vector v = new Vector(magnitude, 0);
			return v;
		}
		else {
			double dist = this.magnitude();
			double rat = magnitude / dist;
			return scale(rat);
		}

	}




	//public String toString();


}

//new Vector(4,3).toString("");