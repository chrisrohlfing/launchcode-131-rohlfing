package exercises4;

import java.awt.Color;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class MouseFollower {

	public static void main(String[] args) {

		StdDraw.setPenColor(Color.GREEN);
		StdDraw.setPenRadius(.05);

		double [] histx = new double [10];
		double [] histy = new double [10];
		int cur = 0;
		
		while (true) {
			StdDraw.clear();
			double x = StdDraw.mouseX();
			double y = StdDraw.mouseY();
			
			histx [cur] = x;
			histy [cur] = y;
			cur = cur + 1;
			if (cur >= 10) {
				cur = 0;
			}
			
			StdDraw.point(histx[cur], histy[cur]);
			//StdDraw.pause(100);
			StdDraw.show(100);
		}

		/*
		StdDraw.setPenColor(Color.GREEN);
		StdDraw.setPenRadius(.05);

		double ballX = 0;
		double ballY = 0;


		while (true) {
			double x = StdDraw.mouseX();
			double y = StdDraw.mouseY();
			//StdDraw.pause(100);

			//while (true) {
			for (int t = 0; t < 10; ++t) {

				StdDraw.clear();
				if (x > ballX) {
					ballX = ballX + .01;
				}
				else {
					ballX = ballX - .01;
				}

				if (y > ballY) {
					ballY = ballY + .01;
				}
				else {
					ballY = ballY - .01;
				}

				StdDraw.point(ballX, ballY);
				StdDraw.show(10);
			}
		}
		 */


		//
		// Render one frame of your animation below here
		//



	}


	// End of your frame
	//
	// Stdraw.show(..) achieves double buffering and
	//   avoids the tight spinning loop



}


