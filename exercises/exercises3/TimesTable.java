package exercises3;

import cse131.ArgsProcessor;

public class TimesTable {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int N = ap.nextInt("Max value for table?");
		
		//  Table should include rows and columns for 0...N  
		//     mkaing N+1 rows and columns
		
		int [][] mult = new int [N+1][N+1];
		for (int i=0; i<N+1; ++i) {
			for (int j=0; j<N+1; ++j) {
				mult [i][j]= i * j;
			}
		}
		System.out.print("    ");
		for (int t = 0; t < N + 1; ++t){
			if (t < 10) {
				System.out.print(" " + t + " ");
			}
			else {
			System.out.print(t + " ");
			}
		}
		System.out.println("");
		
		for (int t = 0; t < N + 2; ++t){
			System.out.print("---");
		}
		//System.out.print("-");
		System.out.println("");
		
		for (int i=0; i < N+1; ++i) {
			System.out.print(i + " | ");
			for (int j = 0; j < N+1; ++j) {
				if (mult [i][j] < 10){
					System.out.print(" " + mult [i][j] + " ");
				}
				else {
				System.out.print(mult [i][j] + " ");
				}
						
			}
			System.out.println("");
		}
		
			
		}
		
		
		

}
