package lab6;

import java.awt.Color;

import sedgewick.StdDraw;

public class triangles {	

	public static void tri(double lx, double ly, double size) {
		
		//double [] x1 = {0 + size, 0 + (size/2), 0};
		//double [] y1 = {0, size * ((Math.sqrt(3)/2)), 0};
		//StdDraw.filledPolygon(x1, y1);
		
		
		if (size < .05) {
			//double [] x = {lx + size/4, lx + size/2, lx + (size/4)};
			//double [] y = {ly + (size * (Math.sqrt(3)/4)), ly, ly + (size * ((Math.sqrt(3)/4)))};
		
			//StdDraw.filledPolygon(x, y);
			return; 
		}
		StdDraw.setPenColor(Color.RED);
		double [] x = {lx + size/4, lx + size/2, lx + (size*3/4)};
		double [] y = {ly + (size * (Math.sqrt(3)/4)), ly, ly + (size * (Math.sqrt(3)/4))};
		StdDraw.filledPolygon(x, y);

		StdDraw.pause(200);
		
		
		
		StdDraw.setPenColor(Color.BLUE);
		
		tri(lx, ly, size/2);

		tri(lx + size/4,ly + size* (Math.sqrt(3)/4), size/2);
		
		tri(lx + size/2, ly, size/2);

	}

	public static void main(String[] args) {

		// StdDraw.setXscale(0, 1);
		// StdDraw.setYscale(0, 1);
		double s= 1; 
		double [] x1 = {0 + s, 0 + (s/2), 0};
		double [] y1 = {0, s * ((Math.sqrt(3)/2)), 0};
		StdDraw.filledPolygon(x1, y1);
		

		StdDraw.setPenColor(Color.RED);
		tri(0,0,1);
		// //double [] x = {lx, lx + size, lx + (size/2)};
		// Once you get things working, you an uncomment the two
		//   calls below to StdDraw.show
		//   and that will speed up what you see greatly
		/*double size = 1;
		double [] x = {0 + size, 0 + (size/2), 0};
		double [] y = {0, size * ((Math.sqrt(3)/2)), 0};
		StdDraw.filledPolygon(x, y);

		 */ // don't show anything StdDraw.show(10); 

		// StdDraw.show(10);  // until now

	}

}
