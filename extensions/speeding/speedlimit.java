package speeding;

import cse131.ArgsProcessor;

public class speedlimit {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor (args);
		int speed = ap.nextInt("How fast do you think you were going?");
		int limit = ap.nextInt("Posted speed limit"); 
		int cost = ((speed - limit) > 9) ? 50 + (speed - limit - 10) * 10 : 0;
		System.out.println("You were going " + speed);
		System.out.println("You owe $" + cost);
		
		// TODO Auto-generated method stub

	}

}
