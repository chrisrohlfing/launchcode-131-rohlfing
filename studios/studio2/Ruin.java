package studio2;

import cse131.ArgsProcessor;

public class Ruin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);

		int startAmount = ap.nextInt("How much money did you bring");
		double odds = ap.nextDouble("What's the % of winning?");
		double winChance = odds / 100;
		int winAmount = ap.nextInt("When ($) will you quit?");
		int totalPlays = ap.nextInt("How many times do we run this simulation?");

		int wins = 0;
		int losses = 0;

		double invodds = 100 - odds;




		for (int i = 1; i <= totalPlays; ++i) {
			int rounds = 0;
			int money = startAmount;
			while (money > 0 && money < winAmount) {
				double spin = Math.random();
				if (spin > winChance) {
					money = money - 1;
				}
				else {
					money = money + 1;
				}
				rounds = rounds + 1;	
				//System.out.println("round" + rounds + " $ " + money);
			}
			System.out.print("Simulation" + i + ": " + rounds + " rounds");
			if (money == 0) {
				System.out.print("     lose");
				losses = losses +1;
			}
			else {
				System.out.print("     win");
				wins = wins + 1;
			}
			System.out.println("");
		}
		System.out.println("losses " + losses);
		System.out.println("wins " + wins);
		System.out.println("simulations " + totalPlays);
		System.out.println("actual ruin rate: " + (double) losses / totalPlays);

		if (odds == invodds) {
			double ruin = 1 - (startAmount / winAmount);
			System.out.println("expected ruin rate: " + ruin);
		}
		else {
			double ruin = (invodds /odds);
			ruin = ((Math.pow(ruin, startAmount)) - (Math.pow(ruin, winAmount))) / (1 - (Math.pow(ruin, winAmount)));			

			System.out.println("expected ruin rate: " + ruin);
		}

	}
}