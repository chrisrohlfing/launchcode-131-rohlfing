package lab5;

public class Lab5Methods {

	public static int sumDownBy2(int n) {
		int ans = 0;
		if (n > 0) {
			for (int i = n; i >= 0; i = i-2) {
				ans = ans + i;
			}
		}
		return ans;
	}

	public static double harmonicSum(int n) {
		double ans = 0;
		if (n > 0) {
			for (double i = 1; i <= n; ++i) {
				ans = ans + (1 / i);
			}
		}
		return ans;
	}

	public static double geometricSum(int k) {
		double ans = 0;
		if (k >= 0) {
			for (double i = 0; i <= k; ++i) {
				ans = ans + (1.0 / Math.pow(2,i));
			}
		}
		return ans;
	}
	public static int mult(int j, int k) {
	//	int ans = 0;
	//	for (int i = 0; i < j; ++i) {
	//		ans = ans + k;
	//	}
		
	
		int ans = multPos (Math.abs(j),Math.abs(k));
		/*if (j < 0) {
			j = j * -1;
		}
		if (k < 0) {
			k = k * -1;
		}
		int ans = 0;
		for (int i = 0; i < j; ++i) {
			ans = ans + k;
		}*/
		if (j < 0 && k > 0) {
			ans = ans * -1;
		}
		else if (j > 0 && k < 0) {
			ans = ans * -1;
		}
		else {
			ans = ans;
		}
	return ans;
}

	public static int expt(int n, int k) {
		int ans = 1;
		if (k < 0) {
			k = k * -1;
		}
		for (int i = 0; i < k; ++i) {
			ans = ans * n;
		}
		return ans;
	}
	
	public static int multPos(int j, int k) {
		/*if (j < 0) {
			j = j * -1;
		}
		if (k < 0) {
			k = k * -1;
		}
		*/
		
		int ans = 0;
		for (int i = 0; i < j; ++i) {
			ans = ans + k;
		}
	return ans;
}

	
public static void main(String[] args) {

	System.out.println(sumDownBy2(7));
	System.out.println(harmonicSum(3));
	System.out.println(geometricSum(2));
	System.out.println(mult(-3,4));
	System.out.println(multPos(3,4));
	System.out.println(expt(2,3));
}
}




